<nav class="navbar navbar-light bg-light">
    <div class="container">
        <a href="/forumsinges"><img src="./data/monkey_forum.png" alt="" width="60px"></a>
        <div>
            <button type="button" class="btn btn-link" id="connexion-button" data-toggle="modal" data-target="#modalConnexion">Connexion</button>
            <button type="button" onclick="decon()" class="btn btn-link deconnexion" id="deconnexion">Déconnexion</button>
        </div>
    </div>
</nav>

<div class="modal fade" id="modalConnexion" tabindex="-1" role="dialog" aria-labelledby="modalConnexion" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Connexion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div id="connexion-done" class="connexion-good">
                <h1>Vous êtes connecté !</h1>
            </div>
            <div class="modal-body">
                <div id="connexion-block">
                    username:
                    <input class="form-control" type="text" value="" id="user">
                    password:
                    <input class="form-control" type="password" value="" id="pwd">
                </div>

            </div>
            <div class="modal-footer">
                <button class="btn btn-link" type="submit" onclick="con()" id="connexion" value="connexion">Connexion</button>
            </div>
        </div>
    </div>
</div>
