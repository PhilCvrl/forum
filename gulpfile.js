'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function (done) {
  gulp.src('src/scss/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('build/css'));
  done();
});

gulp.task('watch', function () {
  gulp.watch('src/**/*.scss', gulp.series('sass'));
});
