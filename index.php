<?php
/**
 * Created by IntelliJ IDEA.
 * User: philippechevreul
 * Date: 28/02/2019
 * Time: 09:29
 */
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FORUM</title>
    <link rel="stylesheet" href="build/css/style.css">
</head>
<body>
<?php
    include_once ('pages/components/navbar.php');
    include_once ('listing-sujet.php');
?>


</body>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.js"></script>
<script src="build/js/script.js"></script>
</html>
