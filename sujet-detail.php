<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FORUM</title>
    <link rel="stylesheet" href="build/css/style.css">
</head>
<body>
<?php
    include_once ('pages/components/navbar.php');
?>
<div class="container">
    <div class="post mx-0">
        <h4>Les Orang-Outans vont-ils disparaîtrent ?</h4>
        <p class="auteur">
            <span>Par rené</span><span>22/03/2019 20:25</span>
        </p>
        <div class="blockquote">
            <p class="mb-0"><small>
                    Bonjour à vous, pensez-vous que les orang-outans peuvent disparaître ? Bisous, René. :)
                </small></p>
        </div>
    </div>
    <div id="disqus_thread"></div>
    <script>

      /**
       *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
       *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
      /*
      var disqus_config = function () {
      this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
      this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
      };
      */
      (function() { // DON'T EDIT BELOW THIS LINE
        var d = document, s = d.createElement('script');
        s.src = 'https://localhost-sy4qtnsolm.disqus.com/embed.js';
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
      })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
</div>
<script id="dsq-count-scr" src="//localhost-sy4qtnsolm.disqus.com/count.js" async></script>
</body>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.js"></script>
<script src="build/js/script.js"></script>
</html>
